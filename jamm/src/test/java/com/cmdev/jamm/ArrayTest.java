package com.cmdev.jamm;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit test for array merge.
 */
public class ArrayTest {
    /**
     * Rigourous Test :-)
     */
    @Test
    public void mergeTwoEqualLengthArrays()
    {
        int[] a = {0, 1, 2, 3, 4};
        int[] b = {5, 6, 7, 8, 9};
        int[] m = ArrayMerger.merge(a, b);
         int[] expected = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
         assertArrayEquals("Arrays don't match", expected, m);
    }

    @Test
    public void mergeTwoDifferentLengthArrays()
    {
        int[] a = {3, 5, 6, 7, 12};
        int[] b = {2, 4, 6, 8};
        int[] m = ArrayMerger.merge(a, b);
        int[] expected = { 2, 3, 4, 5, 6, 6, 7, 8, 12 };
        assertArrayEquals("Arrays don't match", expected, m);
    }

    @Test
    public void oddLengthArrayMedian() {
        assertEquals("wrong median", 3, ArrayMedian.median(new int[]{1, 3, 5}));

    }

    @Test    public void evenArrayMedian() {
        assertEquals("wrong median", 5, ArrayMedian.median(new int[]{1, 3, 4, 6, 7, 9}));
    }

    @Test    public void evenArrayMidMedian() {
        assertEquals("wrong median", 6, ArrayMedian.median(new int[]{1, 3, 5, 7, 7, 9}));
    }
    @Test    public void evenArrayFractionalMedian() {
        assertEquals("wrong median", 65, ArrayMedian.median(new int[]{61, 63, 65, 66, 67, 69}));
    }
}
