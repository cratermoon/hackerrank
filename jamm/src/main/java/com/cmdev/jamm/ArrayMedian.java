package com.cmdev.jamm;

public class ArrayMedian {
    public static int median(int[] a) {
        if (a.length % 2 == 0) { // even number of elements
            int idx = a.length / 2;
            return (a[idx] + a[idx - 1]) / 2;
        } else {
            return a[(a.length - 1) / 2];

        }
    }
}