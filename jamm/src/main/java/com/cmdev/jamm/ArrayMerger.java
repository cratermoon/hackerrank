package com.cmdev.jamm;

public class ArrayMerger {
    public static int[] merge(int[] a, int[]b) {
        if (a.length == 0) {
            return b;
        }
        if (b.length == 0) {
            return a;
        }

        int[] merged = new int[a.length + b.length];
        int indexA = 0;
        int indexB = 0;
        int indexM = 0;

        while (indexA < a.length && indexB < b.length)  {
            if (a[indexA] < b[indexB]) {
                merged[indexM] = a[indexA];
                indexA++;
            } else {
                merged[indexM] = b[indexB];
                indexB++;
            }
            indexM++;
        }
        while (indexA < a.length) {
            merged[indexM] = a[indexA];
            indexA++;
            indexM++;
        }
        while (indexB < b.length) {
            merged[indexM] = b[indexB];
            indexB++;
            indexM++;
        }
        return merged;
    }
}