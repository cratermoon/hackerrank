# HackerRank Answers

## Statement

> Even the best developers forget how to code the most basic things.
> [Whether you're brand new, been coding for a few years, or have made development your lifelong career, you'll run into this scenario and it's totally okay.]( https://twitter.com/kvlly/status/1037146930588135426)


HackerRank tests are total B.S. Even a dev with over 10 years exp,
will open up Knuth or find a library solution to the stupid
exercise-type conundrums. REAL programming ability is recognizing
"Oh, this is the Gas Station Problem", and applying that.

No senior programmer would ever actually need to write code
implementing a basic algorithms class lesson – nor should their
expertise be judged by how well they remember their algorithms
class. Senior developers are supposed to know things like how to
recognize when a problem is a version of "median of two sorted
arrays" and then use the best library for that problem, or even
better, how to recognize which of two ways to solve the problem is
best – the simple version of "merge and then get the median" or the
complex but faster and more space-efficient binary search version.
A senior programmer would be expected to ask questions like "if the
median function returns an integer median of an array of integers,
and the actual median is halfway between two values, should the
result be rounded up or down?" This is **critical** in, for example,
financial calculations, and it wasn't specified in the coding
exercise.

> [In my entire development life I can’t imagine a situation where it would have been an advantage having someone on my team that can spot a line of code that the IDE will balk at before I even press compile.](https://www.linkedin.com/pulse/beware-hackerrank-richard-linnell/)

> [the new performance benchmark set by the coding tests favours university leavers for whom writing esoteric algorithms is second nature. If you’ve spent decades working at the coal face, this kind of thing is a lot less relevant.](https://news.efinancialcareers.com/uk-en/287595/hackerrank-tests-banking)

> [5 Mistakes That Are Ruining Your Chances of Hiring a Great Senior Software Engineer](https://medium.com/@ayasin/5-mistakes-that-are-ruining-your-chances-of-hiring-a-great-senior-software-engineer-4a505471bfc2): Mistake #2 Using Codility, HackerRank, etc.
