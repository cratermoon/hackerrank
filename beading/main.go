package main

import (
	"fmt"
	"math/rand"
	"os"
	"time"
)

const MAXRUN = 3
var colors = []string{ "red",  "orange", "yellow", "green", "blue", "indigo", "violet"}

func bracelet(length int) []string {
	complete := make([]string, length)
	lastThree := make(map[string]bool, 3)
	for i := 0; i < length; i++ {
		nextColor := colors[rand.Intn(len(colors))]
		for possibleColor := colors[rand.Intn(len(colors))] ; lastThree[nextColor] ; {
			nextColor = possibleColor
		}
		complete[i] = nextColor
	}
	return complete
}
func main() {
	rand.Seed(time.Now().UnixNano() * int64(os.Getpid()))
	fmt.Println(bracelet(100))
}
