package main

import (
	"fmt"
	"gitlab.com/cratermoon/arrays"
)

func main() {
	merged := arrays.Merge([]int{1, 3, 5}, []int{0, 2, 4, 6})
	fmt.Println(merged)
	fmt.Println(arrays.Median(merged))

}
