package arrays

// Merge merges 2 sorted arrays
func Merge(a []int, b []int) []int {
	if len(a) == 0 {
		return b
	} else if len(b) == 0 {
		return a
	}
	n := len(a) + len(b)
	result := make([]int, n)

	indexA := 0
	indexB := 0
	indexR := 0
	for indexA < len(a) && indexB < len(b) {
		if a[indexA] < b[indexB] {
			result[indexR] = a[indexA]
			indexA++
		} else {
			result[indexR] = b[indexB]
			indexB++
		}
		indexR++
	}


	for ; indexA < len(a); indexA, indexR = indexA+1, indexR+1 {
		result[indexR] = a[indexA]
	}

	copy(result[indexR:], b[indexB:])
	return result
}
