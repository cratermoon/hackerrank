package arrays

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestArrayMedian(t *testing.T) {
	tests := map[string]struct {
		in       []int
		expected float64
		err      error
	}{
		"one two": {
			in:       []int{1, 2},
			expected: 1.5,
		},
		"odds and evens": {
			in:       []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
			expected: 5,
		},
		"repeated valued": {
			in:       []int{1, 2, 3, 4, 4, 5, 6},
			expected: 4,
		},
		"random set": {
			in:       []int{2, 12, 31, 41, 52, 63, 77, 78, 79, 81, 91},
			expected: 63,
		},
		"an error": {
			in:       []int{},
			expected: 0,
		},
	}

	for name, test := range tests {
		t.Logf("Running test case %s with in %v", name, test.in)
		output := Median(test.in)
		assert.Equal(t, test.expected, output)
	}
}

func TestMergeArrays(t *testing.T) {
	tests := map[string]struct {
		a        []int
		b        []int
		expected []int
	}{
		"one two": {
			a:        []int{1},
			b:        []int{2},
			expected: []int{1, 2},
		},
		"odds and evens": {
			a:        []int{0, 1, 3, 5, 7, 9},
			b:        []int{2, 4, 6, 8, 10},
			expected: []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
		},
		"B longer than A": {
			a:        []int{1, 2, 4},
			b:        []int{3, 4, 5, 6},
			expected: []int{1, 2, 3, 4, 4, 5, 6},
		},
		"A longer than B": {
			a:        []int{1, 2, 4, 8, 16},
			b:        []int{3, 4, 5, 6},
			expected: []int{1, 2, 3, 4, 4, 5, 6, 8, 16},
		},
		"all values in A greater than largest B": {
			a:        []int{8, 16, 32},
			b:        []int{3, 4, 5, 6, 7},
			expected: []int{3, 4, 5, 6, 7, 8, 16, 32},
		},
		"fibs and primes": {
			a:        []int{2, 3, 5, 7, 11},
			b:        []int{0, 1, 1, 2, 3},
			expected: []int{0, 1, 1, 2, 2, 3, 3, 5, 7, 11},
		},
		"both arrays empty": {
			a:        []int{},
			b:        []int{},
			expected: []int{},
		},
		"one array empty": {
			a:        []int{},
			b:        []int{8, 18, 28},
			expected: []int{8, 18, 28},
		},
	}

	for name, test := range tests {
		t.Logf("Running test case %s with ins %v and %v", name, test.a, test.b)
		output := Merge(test.a, test.b)
		assert.Equal(t, test.expected, output)
	}
}
