package arrays

// Median finds the median of an array of ints
func Median(a []int) float64 {
	l := len(a)
	if l == 0 {
		return 0
	}
	i := l - 1
	if l%2 == 0 { // even number of elements
		e1 := float64(a[i/2])
		e2 := float64(a[(i/2)+1])
		return (e1 + e2) / 2
	}
	return float64(a[i/2])
}
